package me.flyray.pay.api;

import java.util.List;

import me.flyray.pay.model.User;

public interface UserService {

	public List<User> displayAllUser();
	
}
